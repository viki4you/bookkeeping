class Journal < ApplicationRecord
  belongs_to :user
  has_many :accounts
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :from_account_number, presence: true
  validates :from_account, presence: true
  validates :from_amount, presence: true
  validates :to_account_number, presence: true
  validates :to_account, presence: true
  validates :to_amount, presence: true
end
