class JournalBackupsController < ApplicationController
  def create(journal)
    @journal_backup = current_user.journal_backups.build(journal_params)
    if @journal_backup.save
      flash[:success] = "Buchung erstellt!"
      redirect_to new_booking_path
    else
      flash[:error] = "Buchung fehlgeschlagen!"
      redirect_to new_booking_path
    end
  end
  
  private

    def journal_params
      params.require(:journal).permit(:buchungsart,
                                      :buchungskreis,
                                      :belegnummer,
                                      :from_account_number,
                                      :from_account,
                                      :from_amount,
                                      :to_account_number,
                                      :to_account,
                                      :to_amount)
    end
end
