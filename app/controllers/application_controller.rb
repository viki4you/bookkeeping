require 'will_paginate/array'

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  # Create Hauptbuch
  def create_hauptbuch
    @journals = current_user.journals
    
    @hauptbuch = Array.new()

    @from_journals = @journals.select(:from_account_number, :from_account, 
                                        :buchungsart)
    @from_journals.each do |journal|
      @hauptbuch << Array.new([journal.from_account_number, journal.from_account,
                                journal.buchungsart, "From"])
    end
    @to_journals = @journals.select(:to_account_number, :to_account,
                                      :buchungsart)
    @to_journals.each do |journal|
      @hauptbuch << Array.new([journal.to_account_number, journal.to_account,
                                journal.buchungsart, "To"])
    end
    @uniq_hauptbuch = @hauptbuch.uniq! {|c| [c[0], c[1]]}
    if @uniq_hauptbuch.nil?
      @hauptbuch = @hauptbuch.uniq
    else
      @hauptbuch = @uniq_hauptbuch
    end
  end
  
  def account(account_name = params[:account_name], 
                account_number = params[:account_number])
    @journals = current_user.journals
    
    @from_account = Array.new()
    @to_account = Array.new()
    
    @journals.reverse.each do |journal|
        if journal.from_account == account_name &&
            journal.from_account_number == account_number
          if journal.buchungsart == "1"
            @to_account << Array.new([journal.to_account_number,
                                      journal.to_account,
                                      journal.to_amount,
                                      journal.buchungsart,"To"
                                      ])
          else
            @from_account << Array.new([journal.to_account_number,
                                      journal.to_account,
                                      journal.to_amount,
                                      journal.buchungsart,
                                      "From"])
          end
        end
        if journal.to_account == account_name &&
            journal.to_account_number == account_number
          if journal.buchungsart == "0"
            @from_account << Array.new([journal.from_account_number,
                                      journal.from_account,
                                      journal.from_amount,
                                      journal.buchungsart,
                                      "To"])
          else
            @to_account << Array.new([journal.from_account_number,
                                      journal.from_account,
                                      journal.from_amount,
                                      journal.buchungsart,
                                      "From"])
          end
        end
    end
    
    @account = Array.new()
    
    @size = @from_account.count
    if @size < @to_account.count
      @size = @to_account.count
    end
    
    @i = 0
    
    until @i >= @size do
      @row = Array.new()
      if !@from_account[@i].nil?
        @row << @from_account[@i][0]
        @row << @from_account[@i][1]
        @row << @from_account[@i][2]
        @row << @from_account[@i][3]
        @row << @from_account[@i][4]
      else  
        @row << '-'
        @row << '-'
        @row << '-'
        @row << '-'
        @row << '-'
      end
      if !@to_account[@i].nil?
        @row << @to_account[@i][0]
        @row << @to_account[@i][1]
        @row << @to_account[@i][2]
        @row << @to_account[@i][3]
        @row << @to_account[@i][4]
      else  
        @row << '-'
        @row << '-'
        @row << '-'
        @row << '-'
        @row << '-'
      end
      @account << @row
      @i += 1
    end
    @account
    #@account = @account.paginate(:page => params[:page], :per_page => 20)
  end

=begin  
  def account
    @journals = current_user.journals
    
    @from_account = Array.new()
    @to_account = Array.new()
    
    @journals.reverse.each do |journal|
        if journal.from_account == params[:account_name] &&
            journal.from_account_number == params[:account_number]
          if journal.buchungsart == "1"
            @to_account << Array.new([journal.to_account_number,
                                      journal.to_account,
                                      journal.to_amount])
          else
            @from_account << Array.new([journal.to_account_number,
                                      journal.to_account,
                                      journal.to_amount])
          end
        end
        if journal.to_account == params[:account_name] &&
            journal.to_account_number == params[:account_number]
          if journal.buchungsart == "0"
            @from_account << Array.new([journal.from_account_number,
                                      journal.from_account,
                                      journal.from_amount])
          else
            @to_account << Array.new([journal.from_account_number,
                                      journal.from_account,
                                      journal.from_amount])
          end
        end
    end
    
    @account = Array.new()
    
    @size = @from_account.count
    if @size < @to_account.count
      @size = @to_account.count
    end
    
    @i = 0
    
    until @i >= @size do
      @row = Array.new()
      if !@from_account[@i].nil?
        @row << @from_account[@i][0]
        @row << @from_account[@i][1]
        @row << @from_account[@i][2]
      else  
        @row << '-'
        @row << '-'
        @row << '-'
      end
      if !@to_account[@i].nil?
        @row << @to_account[@i][0]
        @row << @to_account[@i][1]
        @row << @to_account[@i][2]
      else  
        @row << '-'
        @row << '-'
        @row << '-'
      end
      @account << @row
      @i += 1
    end
    @account
    #@account = @account.paginate(:page => params[:page], :per_page => 20)
  end
=end
  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
end
