class BookingsController < ApplicationController
  before_action :logged_in_user, only: [:create, :new, :show]
  
  def create
    @journal = current_user.journals.build(journal_params)
    
    if current_user.journals.count == 0 &&
      @journal.to_account.to_s != "Eigenkapital"
      flash[:error] = 'Als erste Eroeffnungsbuchung, muss das Eigenkapitalkonto
                        gebucht werden. Hierbei muss der Haben Kontoname den 
                        Eintrag "Eigenkapital" haben.'
      redirect_to new_booking_path
    elsif current_user.journals.count > 0 &&
      @journal.from_account.to_s == "Eigenkapital" ||
      current_user.journals.count > 0 &&
      @journal.to_account.to_s == "Eigenkapital"
        flash[:error] = 'Manuelle Buchungen auf das "Eigenkapital" Konto sind nicht
                        moeglich.'
        redirect_to new_booking_path
    else
      if @journal.save
        flash[:success] = "Buchung erstellt!"
        redirect_to new_booking_path
      else
        flash[:error] = "Buchung fehlgeschlagen!"
        redirect_to new_booking_path
      end
    end
  end
  
  def new
    @journal = current_user.journals.build if logged_in?
  end
  
  def show
    bilanzaccountnumber = "8010"
    eigenkapitalaccountnumber = "30"
    guvaccountnumber = "8030"
    eroeffnungbilanznumber = "8000"
    eroeffnungbilanzname = "Eroeffnungsbilanz"
    @journals = current_user.journals
    @hauptbuch = create_hauptbuch
    @hauptbuch.each do |account|
      @gesamt_betrag = 0
      @soll_betrag = 0
      @haben_betrag = 0
      @account_arr = account(account[1], account[0])
      @account_arr.each do |current_row|
        if current_row[2] != "-"
          @soll_betrag += current_row[2].to_f
        end
        if current_row[7] != "-"
          @haben_betrag += current_row[7].to_f
        end
      end
      @gesamt_betrag = @soll_betrag - @haben_betrag
      if @gesamt_betrag >= 0
        buchungskreis = "AB"
        from_account_number = bilanzaccountnumber
        from_account = "Abschlussbilanz"

        if account[0][0, 1].to_i > 4 && account[0][0, 1].to_i < 8
          buchungskreis = "GuV"
          from_account_number = guvaccountnumber
          from_account = "GuV"
        end

        abschlussbuchung = {  :buchungsart => "10",
                              :buchungskreis => buchungskreis,
                              :belegnummer => "",
                              :from_account_number => from_account_number,
                              :from_account => from_account,
                              :from_amount => @gesamt_betrag,
                              :to_account_number => account[0],
                              :to_account => account[1],
                              :to_amount => @gesamt_betrag
          }
      else
        @gesamt_betrag *= -1
        buchungskreis = "AB"
        to_account_number = bilanzaccountnumber
        to_account = "Abschlussbilanz"

        if account[0][0, 1].to_i > 4 && account[0][0, 1].to_i < 8
          buchungskreis = "GuV"
          to_account_number = guvaccountnumber
          to_account = "GuV"
        end

        abschlussbuchung = {  :buchungsart => "10",
                              :buchungskreis => buchungskreis,
                              :belegnummer => "",
                              :from_account_number => account[0],
                              :from_account => account[1],
                              :from_amount => @gesamt_betrag,
                              :to_account_number => to_account_number,
                              :to_account => to_account,
                              :to_amount => @gesamt_betrag
        }
      end
      
      if account[1].to_s != "Eigenkapital" && account[1].to_s != "Eroeffnungsbilanz"
        current_user.journals.create(abschlussbuchung)
      end
    end
    
    @GuV_arr = account("GuV", guvaccountnumber)
    @soll_betrag = 0
    @haben_betrag = 0
    @GuV_arr.each do |current_row|
      if current_row[2] != "-"
        @soll_betrag += current_row[2].to_i
      end
      if current_row[5] != "-"
        @haben_betrag += current_row[5].to_i
      end
    end
    @gesamt_betrag = @soll_betrag - @haben_betrag
    if @gesamt_betrag >= 0
      abschlussbuchung = {  :buchungsart => "10",
                              :buchungskreis => "Jahresabschluss",
                              :belegnummer => "",
                              :from_account_number => eigenkapitalaccountnumber,
                              :from_account => "Eigenkapital",
                              :from_amount => @gesamt_betrag,
                              :to_account_number => guvaccountnumber,
                              :to_account => "GuV",
                              :to_amount => @gesamt_betrag
      }
    else
      @gesamt_betrag *= -1
      abschlussbuchung = {  :buchungsart => "10",
                              :buchungskreis => "Jahresabschluss",
                              :belegnummer => "",
                              :from_account_number => guvaccountnumber,
                              :from_account => "GuV",
                              :from_amount => @gesamt_betrag,
                              :to_account_number => eigenkapitalaccountnumber,
                              :to_account => "Eigenkapital",
                              :to_amount => @gesamt_betrag
      }
    end
    current_user.journals.create(abschlussbuchung)
    
    # Eigenkapital auf die Schlussbilanz buchen
    @soll_betrag = 0
    @haben_betrag = 0
    @account_arr = account("Eigenkapital", eigenkapitalaccountnumber)
    @account_arr.each do |current_row|
      if current_row[2] != "-"
        @soll_betrag += current_row[2].to_i
      end
      if current_row[5] != "-"
        @haben_betrag += current_row[5].to_i
      end
    end
    @gesamt_betrag = @soll_betrag - @haben_betrag
    abschlussbuchung = {  :buchungsart => "10",
                              :buchungskreis => "Jahresabschluss",
                              :belegnummer => "",
                              :from_account_number => bilanzaccountnumber,
                              :from_account => "Abschlussbilanz",
                              :from_amount => @gesamt_betrag,
                              :to_account_number => guvaccountnumber,
                              :to_account => "Eigenkapital",
                              :to_amount => @gesamt_betrag
    }
    current_user.journals.create(abschlussbuchung)
    current_user.journals.each do |journal|
      journal_temp = {  :buchungsart => journal.buchungsart,
                        :buchungskreis => journal.buchungskreis,
                        :belegnummer => journal.belegnummer,
                        :from_account_number => journal.from_account_number,
                        :from_account => journal.from_account,
                        :from_amount => journal.from_amount,
                        :to_account_number => journal.to_account_number,
                        :to_account => journal.to_account,
                        :to_amount => journal.to_amount,
                        :created_at => journal.created_at,
                        :updated_at => journal.updated_at
                      }
      current_user.journal_backups.create(journal_temp)
    end
    counter_eroeffnung_buchungen = 0
    @eroeffnung = account("Abschlussbilanz", bilanzaccountnumber)
    @eroeffnung.each do |eroeffnung|
      if eroeffnung[1] != "-"
        eroeffnungsbuchung = {  :buchungsart => "11",
                              :buchungskreis => "Eroeffnungsbuchung",
                              :belegnummer => "",
                              :from_account_number => eroeffnung[0],
                              :from_account => eroeffnung[1],
                              :from_amount => eroeffnung[2],
                              :to_account_number => eroeffnungbilanznumber,
                              :to_account => eroeffnungbilanzname,
                              :to_amount => eroeffnung[2]
        }
        current_user.journals.create(eroeffnungsbuchung)
      end
      if eroeffnung[6] != "-"
        eroeffnungsbuchung = {  :buchungsart => "11",
                              :buchungskreis => "Eroeffnungsbuchung",
                              :belegnummer => "",
                              :from_account_number => eroeffnungbilanznumber,
                              :from_account => eroeffnungbilanzname,
                              :from_amount => eroeffnung[2],
                              :to_account_number => eroeffnung[5],
                              :to_account => eroeffnung[6],
                              :to_amount => eroeffnung[2]
        }
        current_user.journals.create(eroeffnungsbuchung)
      end
      
      counter_eroeffnung_buchungen += 1
    end
    current_user.journals.where.not(buchungsart: "11").destroy_all
  end
  
  private

    def journal_params
      params.require(:journal).permit(:buchungsart,
                                      :buchungskreis,
                                      :belegnummer,
                                      :from_account_number,
                                      :from_account,
                                      :from_amount,
                                      :to_account_number,
                                      :to_account,
                                      :to_amount)
    end
end
