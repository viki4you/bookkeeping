class WelcomeController < ApplicationController
  def index
    @journal = current_user.journals.build if logged_in?
  end
end
