class JournalsController < ApplicationController
  before_action :logged_in_user, only: [:create, :show]
  
  def create
    #@journal = current_user.journals.build(journal_params)
    #if @journal.save
    #  flash[:success] = "Buchung erstellt!"
    #  redirect_to new_booking_path
    #else
    #  render 'welcome/index'
    #end
  end
  
  def show
    if params[:account_name].nil?
      #@journals = current_user.journals
    
      #@hauptbuch = Array.new()

      #@from_journals = @journals.select(:from_account_number, :from_account)
      #@from_journals.each do |journal|
      #  @hauptbuch << Array.new([journal.from_account_number, journal.from_account])
      #end
      #@to_journals = @journals.select(:to_account_number, :to_account)
      #@to_journals.each do |journal|
      #  @hauptbuch << Array.new([journal.to_account_number, journal.to_account])
      #end
      #@hauptbuch = @hauptbuch.uniq
      #@hauptbuch = @hauptbuch.paginate(:page => params[:page], :per_page => 20)
      @hauptbuch = create_hauptbuch.paginate(:page => params[:page], :per_page => 20)
    else
      @account = account.paginate(:page => params[:page], :per_page => 20)
    end
  end


  def account
    @journals = current_user.journals
    
    @from_account = Array.new()
    @to_account = Array.new()
    
    @journals.reverse.each do |journal|
        if journal.from_account == params[:account_name] &&
            journal.from_account_number == params[:account_number]
          if journal.buchungsart == "1"
            @to_account << Array.new([journal.to_account_number,
                                      journal.to_account,
                                      journal.to_amount])
          else
            @from_account << Array.new([journal.to_account_number,
                                      journal.to_account,
                                      journal.to_amount])
          end
        end
        if journal.to_account == params[:account_name] &&
            journal.to_account_number == params[:account_number]
          if journal.buchungsart == "0"
            @from_account << Array.new([journal.from_account_number,
                                      journal.from_account,
                                      journal.from_amount])
          else
            @to_account << Array.new([journal.from_account_number,
                                      journal.from_account,
                                      journal.from_amount])
          end
        end
    end
    
    @account = Array.new()
    
    @size = @from_account.count
    if @size < @to_account.count
      @size = @to_account.count
    end
    
    @i = 0
    
    until @i >= @size do
      @row = Array.new()
      if !@from_account[@i].nil?
        @row << @from_account[@i][0]
        @row << @from_account[@i][1]
        @row << @from_account[@i][2]
      else  
        @row << '-'
        @row << '-'
        @row << '-'
      end
      if !@to_account[@i].nil?
        @row << @to_account[@i][0]
        @row << @to_account[@i][1]
        @row << @to_account[@i][2]
      else  
        @row << '-'
        @row << '-'
        @row << '-'
      end
      @account << @row
      @i += 1
    end
    
    @account = @account.paginate(:page => params[:page], :per_page => 20)
  end

  private

    def journal_params
      params.require(:journal).permit(:buchungsart,
                                      :from_account_number,
                                      :from_account,
                                      :from_amount,
                                      :to_account_number,
                                      :to_account,
                                      :to_amount)
    end
end
