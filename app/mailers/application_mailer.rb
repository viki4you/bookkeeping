class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@kibuso.de'
  layout 'mailer'
end
