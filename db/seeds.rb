# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar")
             
users = User.order(:created_at).take(1)
50.times do
  buchungsart = Faker::Number.digit
  from_account_number = Faker::Number.digit
  from_account = Faker::Name.name
  from_amount = Faker::Number.digit
  to_account_number = Faker::Number.digit
  to_account = Faker::Name.name
  to_amount = Faker::Number.digit
  users.each { |user| user.journals.create!(buchungsart: buchungsart,
                                from_account_number: from_account_number,
                                from_account: from_account,
                                from_amount: from_amount,
                                to_account_number: to_account_number,
                                to_account: to_account,
                                to_amount: to_amount) }
end