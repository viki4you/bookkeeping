class AddBelegnummerToJournals < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :belegnummer, :string
  end
end
