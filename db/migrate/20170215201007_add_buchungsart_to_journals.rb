class AddBuchungsartToJournals < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :buchungsart, :string
  end
end
