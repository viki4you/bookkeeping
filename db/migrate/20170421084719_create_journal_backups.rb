class CreateJournalBackups < ActiveRecord::Migration[5.0]
  def change
    create_table :journal_backups do |t|
      t.string :buchungsart
      t.string :buchungskreis
      t.string :belegnummer
      t.string :from_account_number
      t.string :from_account
      t.string :from_amount
      t.string :to_account_number
      t.string :to_account
      t.string :to_amount
      
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
