class AddBuchungskreisToJournals < ActiveRecord::Migration[5.0]
  def change
    add_column :journals, :buchungskreis, :string
  end
end
