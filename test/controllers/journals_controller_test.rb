require 'test_helper'

class JournalsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Journal.count' do
      post journals_path, params: { journal: { from_account_number: "1",
                                              from_account: "Test",
                                              from_amount: "12",
                                              to_account_number: "2",
                                              to_account: "Test2",
                                              to_amount: "12"
      } }
    end
    assert_redirected_to login_url
  end
  
  test "check account created correct" do
    log_in_as(@user)
    from_account_number = "1"
    from_account = "Test"
    to_account = "Test2"
    
    assert_difference 'Journal.count' do  
      post bookings_path, params: { journal: { from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "2",
                                              to_account: to_account,
                                              to_amount: "12"
      } }
    end
    
    get journal_path(@user)
    get journal_path(@user.id, :account_name => from_account,
                      :account_number => from_account_number)
     
    tds = css_select("td")
    c = tds[1].to_s.gsub(/\W+/, '')
    assert c == "td#{to_account}td"
  end
  
  test "show Das Hauptbuch ist leer when journal empty" do
    log_in_as(@user)
    # delete all Journal entries from fixtures
    Journal.destroy_all
    get journal_path(@user)
    assert_select 'h3', 1
  end
end
