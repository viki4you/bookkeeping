require 'test_helper'

class BookingsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  test "should get new" do
    log_in_as(@user)
    get new_booking_path
    assert_response :success
  end
  
  test "should redirect to login if not logged in" do
    get new_booking_path
    assert_redirected_to login_url
  end
  
  test "should redirect to new after booking" do
    log_in_as(@user)
    
    assert_difference 'Journal.count' do
      post bookings_path, params: { journal: { from_account_number: "1",
                                              from_account: "Test",
                                              from_amount: "12",
                                              to_account_number: "2",
                                              to_account: "Test2",
                                              to_amount: "12"
      } }
    end
    assert_equal flash[:success], "Buchung erstellt!"
    assert_redirected_to new_booking_path
  end
  
  test "should redirect to new after booking failed with the right flash massage" do
    log_in_as(@user)
    assert_no_difference 'Journal.count' do
      post bookings_path, params: { journal: { from_account_number: "1",
                                              from_account: "Test",
                                              from_amount: "12",
                                              to_account_number: "2",
                                              to_account: "Test2"
      } }
    end
    assert_equal flash[:error], "Buchung fehlgeschlagen!"
    assert_redirected_to new_booking_path
  end
  
  test "right booking order in account" do
    log_in_as(@user)
    from_account_number = "1"
    from_account = "Test"
    first_booking_account_name = "Test2"
    second_booking_account_name = "Test3"
    post bookings_path, params: { journal: { from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "2",
                                              to_account: first_booking_account_name,
                                              to_amount: "12"
    } }
    post bookings_path, params: { journal: { from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: second_booking_account_name,
                                              to_amount: "12"
    } }
    
    get journal_path(@user.id, :account_name => from_account,
                      :account_number => from_account_number)
    tds = css_select("td")
    c = tds[1].to_s.gsub(/\W+/, '')
    assert c == "td#{first_booking_account_name}td"
    c = tds[7].to_s.gsub(/\W+/, '')
    assert c == "td#{second_booking_account_name}td"
  end
  
  test "booking considering buchungsart with buchungsart normal" do
    log_in_as(@user)
    from_account_number = "1"
    from_account = "Test1"
    to_account_number = "3"
    to_account = "Test2"
    post bookings_path, params: { journal: { buchungsart: "2",
                                              from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: to_account_number,
                                              to_account: to_account,
                                              to_amount: "12"
    } }
    
    get journal_path(@user.id, :account_name => from_account,
                      :account_number => from_account_number)
    tds = css_select("td")
    c = tds[1].to_s.gsub(/\W+/, '')
    assert c == "td#{to_account}td"
    
    get journal_path(@user.id, :account_name => to_account,
                      :account_number => to_account_number)
    tds = css_select("td")
    c = tds[4].to_s.gsub(/\W+/, '')
    assert c == "td#{from_account}td"
  end
  
  test "booking considering buchungsart with buchungsart eroeffnung" do
    log_in_as(@user)
    from_account_number = "1"
    from_account = "Test1"
    to_account_number = "3"
    to_account = "Eroeffnungsbuchung"
    post bookings_path, params: { journal: { buchungsart: "0",
                                              from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: to_account_number,
                                              to_account: to_account,
                                              to_amount: "12"
    } }
    
    get journal_path(@user.id, :account_name => from_account,
                      :account_number => from_account_number)
    tds = css_select("td")
    c = tds[1].to_s.gsub(/\W+/, '')
    assert c == "td#{to_account}td"
    
    get journal_path(@user.id, :account_name => to_account,
                      :account_number => to_account_number)
    tds = css_select("td")
    c = tds[1].to_s.gsub(/\W+/, '')
    assert c == "td#{from_account}td"
  end
  
  test "consider during creating accounts accountnumber and accountname" do
    log_in_as(@user)
    from_account_number = "1"
    from_account = "Test1"
    to_account = "Eroeffnungsbuchung"
    post bookings_path, params: { journal: { buchungsart: "0",
                                              from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: to_account,
                                              to_amount: "12"
    } }
    post bookings_path, params: { journal: { buchungsart: "0",
                                              from_account_number: "2",
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: to_account,
                                              to_amount: "12"
    } }
    get journal_path(@user.id, :account_name => from_account,
                      :account_number => from_account_number)
    trs = css_select("tr")
    assert trs.count == 1
  end
  
  test "check_booking_with_buchungskreis" do
    log_in_as(@user)
    from_account = "Test1"
    to_account = "Test2"
    assert_difference 'Journal.count' do
      post bookings_path, params: { journal: { buchungsart: "2",
                                              buchungskreis: "Kasse",
                                              from_account_number: "2",
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: to_account,
                                              to_amount: "12"
      } }
    end
  end
  
  test "check_booking_with_belegnummer" do
    log_in_as(@user)
    from_account = "Test1"
    to_account = "Test2"
    assert_difference 'Journal.count' do
      post bookings_path, params: { journal: { buchungsart: "2",
                                              buchungskreis: "Kasse",
                                              belegnummer: "ER 125",
                                              from_account_number: "2",
                                              from_account: from_account,
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: to_account,
                                              to_amount: "12"
      } }
    end
  end
  
  test "check_abschluss_journal_buchungen_created" do
=begin    
    log_in_as(@user)
    # delete all Journal entries from fixtures
    create_eigenkapital_buchung
    post bookings_path, params: { journal: { buchungsart: "2",
                                              buchungskreis: "Kasse",
                                              belegnummer: "ER 125",
                                              from_account_number: "2",
                                              from_account: "Test1",
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: "Test2",
                                              to_amount: "12"
    } }
    assert_difference 'Journal.count', 4 do
      get booking_path(@user)
    end
=end
  end
  
  test "check_abschluss_buchungen_calculated_correct" do
=begin
    log_in_as(@user)
    # delete all Journal entries from fixtures
    create_eigenkapital_buchung
    # 2 Buchungen auf zwei gleiche Konten
    post bookings_path, params: { journal: { buchungsart: "2",
                                              buchungskreis: "Kasse",
                                              belegnummer: "ER 125",
                                              from_account_number: "2",
                                              from_account: "Test1",
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: "Test2",
                                              to_amount: "12"
    } }
    post bookings_path, params: { journal: { buchungsart: "2",
                                              buchungskreis: "Kasse",
                                              belegnummer: "ER 125",
                                              from_account_number: "2",
                                              from_account: "Test1",
                                              from_amount: "12",
                                              to_account_number: "3",
                                              to_account: "Test2",
                                              to_amount: "12"
    } }
    assert_difference 'JournalBackup.count', 7 do
      get booking_path(@user)
    end
=end
  end
  
  test "check_abschluss_buchung_created_GuV_instead_of_Abschlussbilanz" do
=begin    
    log_in_as(@user)
    # delete all Journal entries from fixtures
    create_eigenkapital_buchung
    post bookings_path, params: { journal: { buchungsart: "3",
                                              buchungskreis: "Sonstiges",
                                              belegnummer: "AR 125",
                                              from_account_number: "1",
                                              from_account: "Test1",
                                              from_amount: "12",
                                              to_account_number: "2",
                                              to_account: "Test2",
                                              to_amount: "12"
    } }
    post bookings_path, params: { journal: { buchungsart: "4",
                                              buchungskreis: "Sonstiges",
                                              belegnummer: "ER 125",
                                              from_account_number: "3",
                                              from_account: "Test3",
                                              from_amount: "12",
                                              to_account_number: "4",
                                              to_account: "Test4",
                                              to_amount: "12"
    } }
    assert_difference 'Journal.count', 6 do
      get booking_path(@user)
    end
=end
  end
  
  test "check eigenkapital booked first" do
    log_in_as(@user)
    # delete all Journal entries from fixtures
    Journal.destroy_all
    assert_difference 'Journal.count', 0 do
      create_standart_booking
    end
    
    create_eigenkapital_buchung
    assert_difference 'Journal.count', 1 do
      create_standart_booking
    end
  end
end