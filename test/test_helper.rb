ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Returns true if a test user is logged in.
  def is_logged_in?
    !session[:user_id].nil?
  end
  
  # Log in as a particular user.
  def log_in_as(user)
    session[:user_id] = user.id
  end
  
  def create_eigenkapital_buchung(belegnummer = "",
                                  from_account_number = "8",
                                  from_account = "Eroeffnungsbilanz",
                                  from_amount = "12",
                                  to_account_number = "3",
                                  to_account = "Eigenkapital")
    Journal.destroy_all
    post bookings_path, params: { journal: { buchungsart: "7",
                                              buchungskreis: "Eroeffnungsbuchung",
                                              belegnummer: belegnummer,
                                              from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: from_amount,
                                              to_account_number: to_account_number,
                                              to_account: to_account,
                                              to_amount: from_amount
    } }
  end
  
  def create_standart_booking(buchungsart = "2",
                              buchungskreis = "Sonstiges",
                              belegnummer = "ER 125",
                              from_account_number = "3",
                              from_account = "Test3",
                              from_amount = "12",
                              to_account_number = "4",
                              to_account = "Test4",
                              to_amount = "12")
    post bookings_path, params: { journal: { buchungsart: buchungsart,
                                              buchungskreis: buchungskreis,
                                              belegnummer: belegnummer,
                                              from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: from_amount,
                                              to_account_number: to_account_number,
                                              to_account: to_account,
                                              to_amount: to_amount
    } }
  end
  
  def create_booking(buchungsart = "2",
                              buchungskreis = "Sonstiges",
                              belegnummer = "ER 125",
                              from_account_number = "0",
                              from_account = "Test3",
                              from_amount = "12",
                              to_account_number = "3",
                              to_account = "Test4",
                              to_amount = "12")
    post bookings_path, params: { journal: { buchungsart: buchungsart,
                                              buchungskreis: buchungskreis,
                                              belegnummer: belegnummer,
                                              from_account_number: from_account_number,
                                              from_account: from_account,
                                              from_amount: from_amount,
                                              to_account_number: to_account_number,
                                              to_account: to_account,
                                              to_amount: to_amount
    } }
  end
end

class ActionDispatch::IntegrationTest

  # Log in as a particular user.
  def log_in_as(user, password: 'password', remember_me: '1')
    post login_path, params: { session: { email: user.email,
                                          password: password,
                                          remember_me: remember_me } }
  end
end
