require 'test_helper'

class JournalTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @journal = @user.journals.build(from_account_number: "1", 
                          from_account: "test_from_account",
                          from_amount: "12",
                          to_account_number: "2",
                          to_account: "test_to_account",
                          to_amount: "12")
  end

  test "should be valid" do
    assert @journal.valid?
  end

  test "user id should be present" do
    @journal.user_id = nil
    assert_not @journal.valid?
  end
  
  test "content should be present" do
    @journal.from_account_number = "   "
    assert_not @journal.valid?
  end
  
  test "order should be most recent first" do
    assert_equal journals(:most_recent), Journal.first
  end
end
