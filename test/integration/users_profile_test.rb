require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  
  def setup
    @user = users(:michael)
  end

  test "profile display" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
    assert_match @user.journals.count.to_s, response.body
    assert_select 'div.pagination'
    @user.journals.paginate(page: 1).each do |journal|
      assert_match journal.from_account_number, response.body
      assert_match journal.from_account, response.body
      assert_match journal.from_amount, response.body
      assert_match journal.to_account_number, response.body
      assert_match journal.to_account, response.body
      assert_match journal.to_amount, response.body
    end
  end
end
