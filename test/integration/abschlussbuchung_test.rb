require 'test_helper'

class AbschlussbuchungTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
=begin  
  test "booking from beginning till Abschluss" do
    log_in_as(@user)
    Journal.destroy_all
    create_eigenkapital_buchung
    create_standart_booking
    assert_difference "Journal.count", 4 do
      get booking_path(@user)
    end
  end
  
  test "eigenkapital booked only" do
    log_in_as(@user)
    Journal.destroy_all
    create_eigenkapital_buchung
    
    # Journal must increase by 2 (GuV and Abschluss with Eigenkapital)
    assert_difference "Journal.count", 2 do
      get booking_path(@user)
    end
  end
  
  test "abschlussbuchung with eigenkapital_and_aktive_eroeffnungsbuchung" do
    log_in_as(@user)
    from_account = "Kasse"
    create_eigenkapital_buchung
    create_booking("0", "Kasse", "Kein Beleg", "2", from_account, "110",
                    "8", "Eroeffnungsbilanz", "110")
    
    # go to Hauptbuch
    get journal_path(@user.id)
    
    # Selects all tr tags
    trs = css_select("tr")
    assert_equal 3, trs.count
    
    assert_difference "Journal.count", 3 do
      get booking_path(@user)
    end
    assert_equal "Abschlussbilanz", Journal.first.from_account
    assert_equal "Eigenkapital", Journal.first.to_account
    assert_equal "Eigenkapital", Journal.second.from_account
    assert_equal "GuV", Journal.second.to_account
    assert_equal "Abschlussbilanz", Journal.third.from_account
    assert_equal from_account, Journal.third.to_account
  end
  
  test "abschlussbuchung with eigenkapital_and_passive_eroeffnungsbuchung" do
    log_in_as(@user)
    current_account = "Darlehen"
    create_eigenkapital_buchung
    create_booking("1", "Darlehen", "Kein Beleg", "8", "Eroeffnungsbilanz", 
                   "110", "4", current_account, "110")
    
    # go to Hauptbuch
    get journal_path(@user.id)
    
    # Selects all tr tags
    trs = css_select("tr")
    assert_equal trs.count, 3
    
    assert_difference "Journal.count", 3 do
      get booking_path(@user)
    end

    assert_equal "Abschlussbilanz", Journal.first.from_account
    assert_equal "Eigenkapital", Journal.first.to_account
    assert_equal "Eigenkapital", Journal.second.from_account
    assert_equal "GuV", Journal.second.to_account
    assert_equal current_account, Journal.third.from_account
    assert_equal "Abschlussbilanz", Journal.third.to_account
  end
  
  test "abschlussbuchung with ertragskonto im Haben" do
    log_in_as(@user)
    create_eigenkapital_buchung()
    from_account = "Bank"
    to_account = "Miete"
    create_booking("4", "GuV", "ER 129", "2", from_account, "1000", "5", 
                    to_account, "1000")
    get booking_path(@user)
    assert_equal to_account, Journal.third.from_account
    assert_equal "GuV", Journal.third.to_account
    assert_equal "Abschlussbilanz", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
  
  test "abschlussbuchung with ertragskonto im Soll" do
    log_in_as(@user)
    create_eigenkapital_buchung()
    from_account = "Miete"
    to_account = "Bank"
    create_booking("3", "GuV", "ER 129", "5", from_account, "1000", "2", 
                    to_account, "1000")
    get booking_path(@user)
    
    assert_equal to_account, Journal.third.from_account
    assert_equal "Abschlussbilanz", Journal.third.to_account
    assert_equal "GuV", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
  
  test "abschlussbuchung with aufwandskonto im Soll" do
    log_in_as(@user)
    create_eigenkapital_buchung()
    from_account = "Steuern"
    to_account = "Bank"
    create_booking("5", "GuV", "ER 129", "7", from_account, "1000", "2", 
                    to_account, "1000")
    get booking_path(@user)
    
    assert_equal to_account, Journal.third.from_account
    assert_equal "Abschlussbilanz", Journal.third.to_account
    assert_equal "GuV", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
  
  test "abschlussbuchung with aufwandskonto im Haben" do
    log_in_as(@user)
    create_eigenkapital_buchung()
    from_account = "Bank"
    to_account = "Steuern"
    create_booking("5", "GuV", "ER 129", "2", from_account, "1000", "7", 
                    to_account, "1000")
    get booking_path(@user)
    
    assert_equal to_account, Journal.third.from_account
    assert_equal "GuV", Journal.third.to_account
    assert_equal "Abschlussbilanz", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
  
  test "abschlussbuchung check calculation" do
    log_in_as(@user)
    create_eigenkapital_buchung
    from_account = "Bank"
    to_account = "Miete"
    create_booking("5", "GuV", "ER 129", "2", from_account, "1000", "5", 
                    to_account, "1000")
    create_booking("5", "GuV", "ER 130", "2", from_account, "1000", "5", 
                    to_account, "1000")
    get booking_path(@user)
    
    assert_equal to_account, Journal.third.from_account
    assert_equal "GuV", Journal.third.to_account
    assert_equal "2000.0", Journal.third.from_amount
    assert_equal "Abschlussbilanz", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
  
  test "abschlussbuchung check calculation with float" do
    log_in_as(@user)
    create_eigenkapital_buchung
    from_account = "Bank"
    to_account = "Miete"
    create_booking("5", "GuV", "ER 129", "2", from_account, "1000.5", "5", 
                    to_account, "1000")
    create_booking("5", "GuV", "ER 130", "2", from_account, "1000", "5", 
                    to_account, "1000")
    get booking_path(@user)
    
    assert_equal to_account, Journal.third.from_account
    assert_equal "GuV", Journal.third.to_account
    assert_equal "2000.5", Journal.third.from_amount
    assert_equal "Abschlussbilanz", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
  
  test "abschlussbuchung check calculation with float with negativ gesamt_betrag" do
    log_in_as(@user)
    create_eigenkapital_buchung
    from_account = "Bank"
    to_account = "Miete"
    create_booking("5", "GuV", "ER 129", "5", to_account, "1000.5", "2", 
                    from_account, "1000.5")
    create_booking("5", "GuV", "ER 130", "2", from_account, "1001", "5", 
                    to_account, "1001")
    get booking_path(@user)
    
    assert_equal to_account, Journal.third.from_account
    assert_equal "GuV", Journal.third.to_account
    assert_equal "0.5", Journal.third.from_amount
    assert_equal "Abschlussbilanz", Journal.fourth.from_account
    assert_equal from_account, Journal.fourth.to_account
  end
=end
  
  test "journal_backup created and journal cleared" do
    log_in_as(@user)
    create_eigenkapital_buchung
    create_booking
    get booking_path(@user)
    assert JournalBackup.count > 0
    assert Journal.count > 0
  end
  
  test "eroeffnungsbuchungen created" do
    log_in_as(@user)
    create_eigenkapital_buchung
    create_booking
    get booking_path(@user)
    
    assert_equal "11", Journal.first.buchungsart
    assert_equal "0", Journal.first.from_account_number[0, 1]
    assert_equal "Test3", Journal.first.from_account
    
    #puts Journal.first.buchungsart
    #puts Journal.first.from_account_number
    #puts Journal.first.from_account
    #puts Journal.first.from_amount
    
    assert_equal "8", Journal.first.to_account_number[0, 1]
    assert_equal "Eroeffnungsbilanz", Journal.first.to_account
    
    
    #puts Journal.first.to_account_number
    #puts Journal.first.to_account
    #puts Journal.first.to_amount
    
    assert_equal "11", Journal.second.buchungsart
    assert_equal "8", Journal.second.from_account_number[0, 1]
    assert_equal "Eroeffnungsbilanz", Journal.second.from_account
    
    assert_equal "3", Journal.second.to_account_number[0, 1]
    assert_equal "Test4", Journal.second.to_account
    
    
    #puts Journal.second.buchungsart
    #puts Journal.second.from_account_number
    #puts Journal.second.from_account
    #puts Journal.second.from_amount
    
    #puts Journal.second.to_account_number
    #puts Journal.second.to_account
    #puts Journal.second.to_amount
  end
end