require 'test_helper'

class JournalsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  test "show action works properly" do
    log_in_as(@user)
    get "/journals/show"
    assert_response :success
    assert_select "table"
    assert_select "tr"
  end
  
  test "show action shows only unique entries" do
    log_in_as(@user)
    get root_path
    # delete all Journal entries from fixtures
    create_eigenkapital_buchung
    buchungsart = "1"
    from_account_number = "1"
    from_account = "test1"
    from_amount = "12"
    to_account_number = "2"
    to_account = "test2"
    to_amount = "12"
    assert_difference 'Journal.count', 1 do
      post bookings_path, params: { journal: {
                                    buchungsart: buchungsart,
                                    from_account_number: from_account_number,
                                    from_account: from_account,
                                    from_amount: from_amount,
                                    to_account_number: to_account_number,
                                    to_account: to_account,
                                    to_amount: to_amount
      } }
    end
    assert_difference 'Journal.count', 1 do
      post bookings_path, params: { journal: {
                                    buchungsart: buchungsart,
                                    from_account_number: from_account_number,
                                    from_account: from_account,
                                    from_amount: from_amount,
                                    to_account_number: to_account_number,
                                    to_account: to_account,
                                    to_amount: to_amount
      } }
    end
    get journal_path(@user.id)
    assert_select 'tr', 4
  end
end
