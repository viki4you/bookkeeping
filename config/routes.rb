Rails.application.routes.draw do
  get 'bookings/create'

  get 'password_resets/new'

  get 'password_resets/edit'

  root  'welcome#index'

  get     '/signup',  to: 'users#new'
  get     '/login',   to: 'sessions#new'
  post    '/login',   to: 'sessions#create'
  delete  '/logout',  to: 'sessions#destroy'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :journals,            only: [:create, :show]
  resources :bookings
  
  #get ':controller(/:action(/:id))'
end
