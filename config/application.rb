require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Bookkeeping
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
=begin
    config.generators do |g|
      g.factory_girl false
    end
    
    config.generators do |g|
      g.factory_girl dir: 'spec/support'
    end

    config.generators do |g|
      g.test_framework :rspec
    end
=end
  end
end